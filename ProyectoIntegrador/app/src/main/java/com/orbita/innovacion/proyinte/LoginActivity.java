package com.orbita.innovacion.proyinte;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.File;
import java.io.IOException;

/**
 * Clase que se encargará de los inicios de sesión de los padres de familia dados de alta
 * y personal de comedor si la institucion cuenta con tal departamento.
 *
 * Fecha de creación: 11/02/2018.
 * Modificaciones:
 * Integración de video de fondo para BackGround para mejor vista de la ventana.
 * Implementación de ventana de dialogo para despliegue de los campos de Login.
 */

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private ConstraintLayout myLayout;

    private GoogleApiClient googleApiClient;
    private Button signInButton, empleados;

    public static final int SIG_IN_CODE = 777;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    private ImageView imageView;
    private ProgressBar progressBar;
    private VideoView mVideoView;

    AlphaAnimation fadeOut;
    AlphaAnimation fadeIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (Build.VERSION.SDK_INT >= 21){
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        fadeIn = new AlphaAnimation(0.0f, 1.0f);
        fadeIn.setDuration(1500);
        fadeIn.setStartOffset(2000);
        fadeIn.setFillAfter(true);

        fadeOut = new AlphaAnimation(1.0f, 0.0f);
        fadeOut.setDuration(1500);
        fadeOut.setStartOffset(2000);
        fadeOut.setFillAfter(true);

        mVideoView = (VideoView) findViewById(R.id.bgVideoView);

        video();

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setLooping(true);
            }
        });

        myLayout = (ConstraintLayout) findViewById(R.id.myLayout);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        empleados = (Button) findViewById(R.id.Btn);
        signInButton = (Button) findViewById(R.id.SigInButton);
        imageView = (ImageView) findViewById(R.id.orImageView);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInButton.startAnimation(fadeIn);
                empleados.startAnimation(fadeIn);
                imageView.startAnimation(fadeIn);
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent, SIG_IN_CODE);

                desaparecer();
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();

        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null){
                    goMainScreen();
                }
            }
        };

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        mVideoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                signInButton.startAnimation(fadeIn);
                empleados.startAnimation(fadeIn);
                imageView.startAnimation(fadeIn);

                desaparecer();

                return true;
            }
        });

        empleados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInButton.startAnimation(fadeIn);
                empleados.startAnimation(fadeIn);
                imageView.startAnimation(fadeIn);
                ventanaDialogo();

                desaparecer();
            }
        });

        desaparecer();
    }

    private void desaparecer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                signInButton.startAnimation(fadeOut);
                empleados.startAnimation(fadeOut);
                imageView.startAnimation(fadeOut);
            }
        }, 6000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SIG_IN_CODE){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSingInResult(result);
        }
    }

    private void handleSingInResult(GoogleSignInResult result) {
        if (result.isSuccess()){
            firebaseAuthWithGoogle(result.getSignInAccount());
            goMainScreen();
        }else{
            Snackbar.make(myLayout, "No se pudo inicar sesión",
                    Snackbar.LENGTH_LONG).setAction("", null).show();
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount signInAccount) {
        progressBar.setVisibility(View.VISIBLE);
        signInButton.setVisibility(View.GONE);
        AuthCredential credential = GoogleAuthProvider.getCredential(signInAccount.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);
                signInButton.setVisibility(View.VISIBLE);
                if(!task.isSuccessful()){
                    Snackbar.make(myLayout, "No se pudo autenticar con Firebase",
                            Snackbar.LENGTH_LONG).setAction("", null).show();
                }
            }
        });
    }

    /*Método encargado de abrir la ventana principal de la aplicación*/
    private void goMainScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(firebaseAuthListener != null){
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        video();
    }

    private void video() {
        Uri uri = Uri.parse("https://drive.google.com/uc?export=download&id=1hsT1hJ9cxQ2JDO4-z14TBFgWRC6M6TN3");
        mVideoView.setVideoURI(uri);
        mVideoView.start();
    }

    public void ventanaDialogo()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Aplicación para empleados");
        builder.setMessage("¿Es usted un empleado de la institución?");

        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Snackbar.make(findViewById(R.id.myLayout), "Acción Desechada",
                                Snackbar.LENGTH_LONG).setAction("", null).show();

                    }
                });

        builder.setPositiveButton("Si",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        startNewActivity(LoginActivity.this);

                    }
                });
        builder.show();

    }

    public void startNewActivity(Context context) {
        String packageName = getString(R.string.SVAEmpleados);
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + packageName));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

}
