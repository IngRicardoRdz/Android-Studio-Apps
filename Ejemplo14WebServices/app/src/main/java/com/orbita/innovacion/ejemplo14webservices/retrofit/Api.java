package com.orbita.innovacion.ejemplo14webservices.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.*;

public interface Api {
    String URL = "http://simplifiedcoding.net/demos/";

    @GET("marvel")
    Call<List<Marvel>> getAll();
}
