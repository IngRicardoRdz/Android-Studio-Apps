package com.orbita.innovacion.ejemplo12mysqlite1tabla;

/**
 * Created by ricardo on 5/03/18.
 */

public class Contactos {

    private int id;
    private String nombre, phone;
    private boolean genero;

    public Contactos(int id, String nombre, String phone, boolean genero) {
        this.id = id;
        this.nombre = nombre;
        this.phone = phone;
        this.genero = genero;
    }

    public Contactos() {
        this(0, "Juana Perez", "", false);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean getGenero() {
        return genero;
    }

    public void setGenero(boolean genero) {
        this.genero = genero;
    }
}
