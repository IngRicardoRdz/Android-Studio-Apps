package com.orbita.innovacion.ejemplo11notificaciones;

import android.app.NotificationManager;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class App extends AppCompatActivity {

    Button simple;
    String titulo, contenido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app);

        simple = (Button) findViewById(R.id.btnSimple);

        simple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificacion();
                titulo = "Notificacion Simple";
                contenido = "Esta es una notificaciones de la aplicacion";
            }
        });

    }

    public void notificacion(){
        NotificationCompat.Builder mBuilder;
        NotificationManager mNotifyMgr =(NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        mBuilder =new NotificationCompat.Builder(getApplicationContext())
                //.setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon((((BitmapDrawable) getResources().getDrawable(R.drawable.yin_yang)).getBitmap()))
                .setSound(soundUri)
                .setContentTitle(titulo)
                .setContentText(contenido)
                .setVibrate(new long[] {100, 250, 100, 500})
                .setAutoCancel(true);

        mNotifyMgr.notify(1, mBuilder.build());
    }

}
